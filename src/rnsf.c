/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* strtok_r */

#define PI_EPSILON 1.0e-4

#include "rnsf.h"
#include "rnsf_c.h"
#include "rnsf_log.h"

#include <rsys/cstr.h>
#include <rsys/math.h>
#include <rsys/text_reader.h>

#include <math.h>
#include <string.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
check_rnsf_create_args(const struct rnsf_create_args* args)
{
  /* Nothing to check. Only return RES_BAD_ARG if args is NULL */
  return args ? RES_OK : RES_BAD_ARG;
}

static void
normalize_phase_fn_discret(struct phase_fn_discrete* discrete)
{
  struct rnsf_discrete_item* items = NULL;
  size_t i, n;
  double phi_prev;
  double alpha;
  ASSERT(discrete);

  n = darray_discrete_item_size_get(&discrete->items);
  ASSERT(n >= 2);

  items = darray_discrete_item_data_get(&discrete->items);
  ASSERT(items[0].theta == 0 && items[n-1].theta == PI);

  alpha = 0;
  phi_prev = 1;
  FOR_EACH(i, 1, n) {
    const double phi_curr = cos(items[i].theta);
    ASSERT(phi_prev > phi_curr);
    alpha += (items[i-1].value + items[i].value) * (phi_prev - phi_curr);
    phi_prev = phi_curr;
  }
  alpha *= PI;

  if(!eq_eps(alpha, 1, 1.e-6)) {
    FOR_EACH(i, 0, n) items[i].value /= alpha;
  }
}

static res_T
parse_phase_fn_hg
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   char** tk_ctx,
   struct phase_fn_hg* phase)
{
  char* tk = NULL;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && tk_ctx && phase);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(rnsf,
      "%s:%lu: missing the Henyey & Greenstein asymmetric parameter.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_double(tk, &phase->g);
  if(res == RES_OK && (phase->g < -1 || phase->g > 1)) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(rnsf,
      "%s:%lu: invalid Henyey & Greenstein asymmetric parameter `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(rnsf, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_discrete_item
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   struct rnsf_discrete_item* item)
{
  char* tk = NULL;
  char* tk_ctx = NULL;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && item);

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(rnsf, "%s: could not read the line `%lu' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) {
    log_err(rnsf, "%s:%lu: missing an item of the discretized phase function.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  res = cstr_to_double(tk, &item->theta);
  if(res == RES_OK 
  && (  item->theta < 0 
     || (!eq_eps(item->theta, PI, PI_EPSILON) && item->theta > PI))) {
    res = RES_BAD_ARG;
  }
  if(res != RES_OK) {
    log_err(rnsf, "%s:%lu: invalid phase function angle `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(!tk) {
    log_err(rnsf,
      "%s:%lu: the value of the discretized phase function is missing.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_double(tk, &item->value);
  if(res == RES_OK && item->value < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(rnsf, "%s:%lu: invalid value `%s' of the discretized phase function.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(tk) {
    log_warn(rnsf, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_phase_fn_discrete
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   char** tk_ctx,
   struct phase_fn_discrete* phase)
{
  char* tk = NULL;
  unsigned long nangles = 0;
  unsigned long i;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && tk_ctx && phase);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(rnsf,
      "%s:%lu: missing the angle number of the discretized phase function.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_ulong(tk, &nangles);
  if(res == RES_OK && nangles < 2) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(rnsf,
      "%s:%lu: invalid angle number of the discretized phase function `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  res = darray_discrete_item_resize(&phase->items, nangles);
  if(res != RES_OK) {
    log_err(rnsf,
      "%s:%lu: could not allocate the list of values of the discretized "
      "phase function -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(rnsf, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  FOR_EACH(i, 0, nangles) {
    struct rnsf_discrete_item* item = NULL;
    item = darray_discrete_item_data_get(&phase->items) + i;

    res = parse_discrete_item(rnsf, txtrdr, item);
    if(res != RES_OK) goto error;

    if(i == 0 && item->theta != 0) {
      log_err(rnsf,
        "%s:%lu: invalid angle value `%g'. The first angle must be 0.\n",
        txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
        item->theta);
      res = RES_BAD_ARG;
      goto error;
    }

    if(i == nangles - 1) {
      if(eq_eps(item->theta, PI, PI_EPSILON)) {
        item->theta = PI;
      } else {
        log_err(rnsf,
          "%s:%lu: invalid angle value `%g'. The last angle must be 3.14159.\n",
          txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
          item->theta);
        res = RES_BAD_ARG;
        goto error;
      }
    }

    if(i > 0 && item[0].theta <= item[-1].theta) {
      log_err(rnsf,
        "%s:%lu: the discretized phase function must be sorted in ascending "
        "order of angles (item %lu at %g rad; item %lu at %g rad).\n",
        txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
        (unsigned long)(i-1), item[-1].theta,
        (unsigned long)(i),   item[ 0].theta);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_phase_fn
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   char** tk_ctx,
   struct rnsf_phase_fn* phase)
{
  char* tk = NULL;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && tk_ctx && phase);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(rnsf, "%s:%lu: missing phase function type.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  if(!strcmp(tk, "HG")) {
    phase->type = RNSF_PHASE_FN_HG;
    res = parse_phase_fn_hg(rnsf, txtrdr, tk_ctx, &phase->param.hg);
    if(res != RES_OK) goto error;

  } else if(!strcmp(tk, "discrete")) {
    phase->type = RNSF_PHASE_FN_DISCRETE;
    phase_fn_discrete_init(rnsf->allocator, &phase->param.discrete);
    res = parse_phase_fn_discrete(rnsf, txtrdr, tk_ctx, &phase->param.discrete);
    if(res != RES_OK) goto error;

    normalize_phase_fn_discret(&phase->param.discrete);

  } else {
    log_err(rnsf, "%s:%lu: invalid phase function type `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}


static res_T
parse_wlen_phase_fn
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   struct rnsf_phase_fn* phase)
{
  char* tk = NULL;
  char* tk_ctx = NULL;
  double wlen = 0;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && phase);

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(rnsf, "%s: could not read the line `%lu' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) {
    const size_t nexpect = darray_phase_fn_size_get(&rnsf->phases);
    const size_t nparsed = (size_t)
      (phase - darray_phase_fn_cdata_get(&rnsf->phases));
    log_err(rnsf,
      "%s:%lu: missing phase function. "
      "Expecting %lu function%swhile %lu %s parsed.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      (unsigned long)nexpect, nexpect == 1 ? " " : "s ",
      (unsigned long)nparsed, nparsed > 1 ? "were" : "was");
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  res = cstr_to_double(tk, &wlen);
  if(res == RES_OK && wlen < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(rnsf, "%s:%lu: invalid wavelength `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }
  phase->wlen[0] = wlen;
  phase->wlen[1] = wlen;

  res = parse_phase_fn(rnsf, txtrdr, &tk_ctx, phase);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_per_wlen_phase_fn
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   char** tk_ctx)
{
  char* tk = NULL;
  unsigned long count = 0;
  unsigned long i;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && tk_ctx);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(rnsf, "%s:%lu: the number of wavelengths is missing.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_ulong(tk, &count);
  if(res != RES_OK) {
    log_err(rnsf, "%s:%lu: invalid number of wavelengths `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(rnsf, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  res = darray_phase_fn_resize(&rnsf->phases, count);
  if(res != RES_OK) {
    log_err(rnsf,
      "%s:%lu: could not allocate the list of phase functions -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  FOR_EACH(i, 0, count) {
    struct rnsf_phase_fn* phase = NULL;

    phase = darray_phase_fn_data_get(&rnsf->phases) + i;

    res = parse_wlen_phase_fn(rnsf, txtrdr, phase);
    if(res != RES_OK) goto error;
    ASSERT(phase->wlen[0] == phase->wlen[1]);

    if(i > 0 && phase[0].wlen[0] <= phase[-1].wlen[0]) {
      log_err(rnsf,
        "%s:%lu: the phase functions must be sorted in ascending order of "
        "wavelengths (phase function %lu at %g nm; phase function %lu at %g nm).\n",
        txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
        (unsigned long)(i-1), phase[-1].wlen[0],
        (unsigned long)(i),   phase[ 0].wlen[0]);
      res = RES_BAD_ARG;
      goto error;
    }
  }

exit:
  return res;
error:
  darray_phase_fn_clear(&rnsf->phases);
  goto exit;
}

static res_T
parse_band_phase
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   struct rnsf_phase_fn* phase)
{
  char* tk = NULL;
  char* tk_ctx = NULL;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && phase);

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(rnsf, "%s: could not read the line `%lu' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) {
    const size_t nexpect = darray_phase_fn_size_get(&rnsf->phases);
    const size_t nparsed = (size_t)
      (phase - darray_phase_fn_cdata_get(&rnsf->phases));
    log_err(rnsf,
      "%s:%lu: missing phase function. "
      "Expecting %lu function%swhile %lu %s parsed.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      (unsigned long)nexpect, nexpect == 1 ? " " : "s ",
      (unsigned long)nparsed, nparsed > 1 ? "were" : "was");
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  res = cstr_to_double(tk, &phase->wlen[0]);
  if(res == RES_OK && phase->wlen[0] < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(rnsf, "%s:%lu: invalid band min boundary `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(!tk) {
    log_err(rnsf, "%s:%lu: missing band max boundary.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_double(tk, &phase->wlen[1]);
  if(res == RES_OK && phase->wlen[1] < 0) res = RES_BAD_ARG;
  if(res != RES_OK) {
    log_err(rnsf, "%s:%lu: invalid band max boundary `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    goto error;
  }

  if(phase->wlen[0] > phase->wlen[1]) {
    log_err(rnsf, "%s:%lu: band boundaries are degenerated -- [%g, %g].\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      phase->wlen[0], phase->wlen[1]);
    res = RES_BAD_ARG;
    goto error;
  }

  res = parse_phase_fn(rnsf, txtrdr, &tk_ctx, phase);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_per_band_phase_fn
  (struct rnsf* rnsf,
   struct txtrdr* txtrdr,
   char** tk_ctx)
{
  char* tk = NULL;
  unsigned long count = 0;
  unsigned long i = 0;
  res_T res = RES_OK;
  ASSERT(rnsf && txtrdr && tk_ctx);

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(!tk) {
    log_err(rnsf, "%s:%lu: the number of bands is missing.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = cstr_to_ulong(tk, &count);
  if(res != RES_OK) {
    log_err(rnsf, "%s:%lu: invalid number of bands `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(NULL, " \t", tk_ctx);
  if(tk) {
    log_warn(rnsf, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  res = darray_phase_fn_resize(&rnsf->phases, count);
  if(res != RES_OK) {
    log_err(rnsf,
      "%s:%lu: could not allocate the list of phase functions -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  FOR_EACH(i, 0, count) {
    struct rnsf_phase_fn* phase = NULL;

    phase = darray_phase_fn_data_get(&rnsf->phases) + i;

    res = parse_band_phase(rnsf, txtrdr, phase);
    if(res != RES_OK) goto error;

    if(i > 0) {
      if(phase[0].wlen[0] <  phase[-1].wlen[1]
      || phase[0].wlen[0] == phase[-1].wlen[0]) {
        log_err(rnsf,
          "%s:%lu: the phase functions must be sorted in ascending order of "
          "wavelengths and must not overlap (phase function %lu in [%g, %g] nm; "
          "phase function %lu in [%g %g] nm).\n",
          txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
          (unsigned long)(i-1), phase[-1].wlen[0], phase[-1].wlen[1],
          (unsigned long)(i),   phase[ 0].wlen[0], phase[ 0].wlen[1]);
        res = RES_BAD_ARG;
        goto error;
      }
    }
  }

exit:
  return res;
error:
  darray_phase_fn_clear(&rnsf->phases);
  goto exit;
}

static res_T
load_stream
  (struct rnsf* rnsf,
   FILE* stream,
   const char* stream_name)
{
  char* tk = NULL;
  char* tk_ctx = NULL;
  struct txtrdr* txtrdr = NULL;
  res_T res = RES_OK;
  ASSERT(rnsf && stream && stream_name);

  darray_phase_fn_clear(&rnsf->phases); /* Clean-up */

  res = txtrdr_stream(rnsf->allocator, stream, stream_name, '#', &txtrdr);
  if(res != RES_OK) {
    log_err(rnsf, "Could not create the text reader to parse `%s' -- %s.\n",
      stream_name, res_to_cstr(res));
    goto error;
  }

  res = txtrdr_read_line(txtrdr);
  if(res != RES_OK) {
    log_err(rnsf, "%s: could not read the line `%lu' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      res_to_cstr(res));
    goto error;
  }

  if(!txtrdr_get_cline(txtrdr)) goto exit; /* No line to parse */

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  ASSERT(tk);

  if(!strcmp(tk, "wavelengths")) {
    res = parse_per_wlen_phase_fn(rnsf, txtrdr, &tk_ctx);
    if(res != RES_BAD_ARG) goto error;
  } else if(!strcmp(tk, "bands")) {
    res = parse_per_band_phase_fn(rnsf, txtrdr, &tk_ctx);
    if(res != RES_BAD_ARG) goto error;
  } else {
    log_err(rnsf, "%s:%lu: invalid directive `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
    res = RES_BAD_ARG;
    goto error;
  }

exit:
  if(txtrdr) txtrdr_ref_put(txtrdr);
  return res;
error:
  goto exit;
}

static void
release_rnsf(ref_T* ref)
{
  struct rnsf* rnsf = NULL;
  ASSERT(ref);
  rnsf = CONTAINER_OF(ref, struct rnsf, ref);
  if(rnsf->logger == &rnsf->logger__) logger_release(&rnsf->logger__);
  darray_phase_fn_release(&rnsf->phases);
  MEM_RM(rnsf->allocator, rnsf);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
rnsf_create
  (const struct rnsf_create_args* args,
   struct rnsf** out_rnsf)
{
  struct rnsf* rnsf = NULL;
  struct mem_allocator* allocator = NULL;
  res_T res = RES_OK;

  if(!out_rnsf) { res = RES_BAD_ARG; goto error; }
  res = check_rnsf_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  rnsf = MEM_CALLOC(allocator, 1, sizeof(*rnsf));
  if(!rnsf) {
    if(args->verbose) {
      #define ERR_STR "Could not allocate the device of the "\
        "Rad-Net Scattering Function library.\n"
      if(args->logger) {
        logger_print(args->logger, LOG_ERROR, ERR_STR);
      } else {
        fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
      }
      #undef ERR_STR
    }
    res = RES_MEM_ERR;
    goto error;
  }

  ref_init(&rnsf->ref);
  rnsf->allocator = allocator;
  rnsf->verbose = args->verbose;
  darray_phase_fn_init(allocator, &rnsf->phases);
  if(args->logger) {
    rnsf->logger = args->logger;
  } else {
    res = setup_log_default(rnsf);
    if(res != RES_OK) {
      if(args->verbose) {
        fprintf(stderr, MSG_ERROR_PREFIX
          "Could not setup the default logger "
          "of the Rad-Net Scattering Function library.\n");
      }
      goto error;
    }
  }

exit:
  if(out_rnsf) *out_rnsf = rnsf ;
  return res;
error:
  if(rnsf) { RNSF(ref_put(rnsf)); rnsf = NULL; }
  goto exit;
}

res_T
rnsf_ref_get(struct rnsf* rnsf)
{
  if(!rnsf) return RES_BAD_ARG;
  ref_get(&rnsf->ref);
  return RES_OK;
}

res_T
rnsf_ref_put(struct rnsf* rnsf)
{
  if(!rnsf) return RES_BAD_ARG;
  ref_put(&rnsf->ref, release_rnsf);
  return RES_OK;
}

res_T
rnsf_load(struct rnsf* rnsf, const char* filename)
{
  FILE* fp = NULL;
  res_T res = RES_OK;

  if(!rnsf || !filename) {
    res = RES_BAD_ARG;
    goto error;
  }

  fp = fopen(filename, "r");
  if(!fp) {
    log_err(rnsf, "%s: error opening file `%s' -- %s.\n",
      FUNC_NAME, filename, strerror(errno));
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(rnsf, fp, filename);
  if(res != RES_OK) goto error;

exit:
  if(fp) fclose(fp);
  return res;
error:
  goto exit;
}

res_T
rnsf_load_stream
  (struct rnsf* rnsf,
   FILE* stream,
   const char* stream_name)
{
  res_T res = RES_OK;

  if(!rnsf || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = load_stream(rnsf, stream, stream_name ? stream_name : "<stream>");
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

size_t
rnsf_get_phase_fn_count(const struct rnsf* rnsf)
{
  ASSERT(rnsf);
  return darray_phase_fn_size_get(&rnsf->phases);
}

const struct rnsf_phase_fn*
rnsf_get_phase_fn(const struct rnsf* rnsf, const size_t iphase_fn)
{
  ASSERT(rnsf && iphase_fn < rnsf_get_phase_fn_count(rnsf));
  return darray_phase_fn_cdata_get(&rnsf->phases) + iphase_fn;
}

enum rnsf_phase_fn_type
rnsf_phase_fn_get_type(const struct rnsf_phase_fn* phase)
{
  ASSERT(phase);
  return phase->type;
}

res_T
rnsf_phase_fn_get_hg
  (const struct rnsf_phase_fn* phase,
   struct rnsf_phase_fn_hg* hg)
{
  if(!phase || !hg) return RES_BAD_ARG;
  if(phase->type != RNSF_PHASE_FN_HG) return RES_BAD_ARG;
  hg->wavelengths[0] = phase->wlen[0];
  hg->wavelengths[1] = phase->wlen[1];
  hg->g = phase->param.hg.g;
  return RES_OK;
}

res_T
rnsf_phase_fn_get_discrete
  (const struct rnsf_phase_fn* phase,
   struct rnsf_phase_fn_discrete* discrete)
{
  if(!phase || !discrete) return RES_BAD_ARG;
  if(phase->type != RNSF_PHASE_FN_DISCRETE) return RES_BAD_ARG;
  discrete->wavelengths[0] = phase->wlen[0];
  discrete->wavelengths[1] = phase->wlen[1];
  discrete->items = darray_discrete_item_cdata_get(&phase->param.discrete.items);
  discrete->nitems = darray_discrete_item_size_get(&phase->param.discrete.items);
  return RES_OK;
}
