/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnsf.h"

#include <rsys/math.h>
#include <rsys/mem_allocator.h>

#include <stdio.h>

static void
test_load1(struct rnsf* rnsf)
{
  FILE* fp = NULL;
  const char* filename = "test_file_bands.rnsf";
  const struct rnsf_phase_fn* phase = NULL;
  struct rnsf_phase_fn_hg hg = RNSF_PHASE_FN_HG_NULL;

  CHK(fp = fopen(filename, "w+"));
  fprintf(fp, "# Comment\n");
  fprintf(fp, "bands 1\n");
  fprintf(fp, "\n");
  fprintf(fp, "200.1 280.3 HG 0\n");
  rewind(fp);

  CHK(rnsf_load_stream(NULL, fp, filename) == RES_BAD_ARG);
  CHK(rnsf_load_stream(rnsf, NULL, filename) == RES_BAD_ARG);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);

  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, filename) == RES_OK);

  CHK(rnsf_load(NULL, filename) == RES_BAD_ARG);
  CHK(rnsf_load(rnsf, NULL) == RES_BAD_ARG);
  CHK(rnsf_load(rnsf, "invalid") == RES_IO_ERR);
  CHK(rnsf_load(rnsf, filename) == RES_OK);

  CHK(rnsf_get_phase_fn_count(rnsf) == 1);
  CHK(phase = rnsf_get_phase_fn(rnsf, 0));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);

  CHK(rnsf_phase_fn_get_hg(NULL, &hg) == RES_BAD_ARG);
  CHK(rnsf_phase_fn_get_hg(phase, NULL) == RES_BAD_ARG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 200.1);
  CHK(hg.wavelengths[1] == 280.3);
  CHK(hg.g == 0);
  CHK(fclose(fp) == 0);
}

static void
test_load2(struct rnsf* rnsf)
{
  struct rnsf_phase_fn_discrete discrete = RNSF_PHASE_FN_DISCRETE_NULL;
  struct rnsf_phase_fn_hg hg = RNSF_PHASE_FN_HG_NULL;
  FILE* fp = NULL;
  const struct rnsf_phase_fn* phase = NULL;

  CHK(fp = tmpfile());
  fprintf(fp, "bands 0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(rnsf_get_phase_fn_count(rnsf) == 0);
  CHK(fclose(fp) == 0);

  CHK(fp = tmpfile());
  fprintf(fp, "bands 5\n");
  fprintf(fp, "100 200  HG -0.3\n");
  fprintf(fp, "200 300  HG  0.4\n");
  fprintf(fp, "300 400  HG  0.5\n");
  fprintf(fp, "850 875.123  discrete 5\n");
  fprintf(fp, " 0  0.3\n");
  fprintf(fp, " %.9g 0.3\n", PI/4.0);
  fprintf(fp, " %.9g 0.3\n", PI/2.0);
  fprintf(fp, " %.9g 0.3\n", 3*PI/4.0);
  fprintf(fp, " 3.14159 0.3\n");
  fprintf(fp, "900 900 HG -0.1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);

  CHK(rnsf_get_phase_fn_count(rnsf) == 5);

  CHK(phase = rnsf_get_phase_fn(rnsf, 0));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_discrete(phase, &discrete) == RES_BAD_ARG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 100 && hg.wavelengths[1] == 200);
  CHK(hg.g == -0.3);

  CHK(phase = rnsf_get_phase_fn(rnsf, 1));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 200 && hg.wavelengths[1] == 300);
  CHK(hg.g == 0.4);

  CHK(phase = rnsf_get_phase_fn(rnsf, 2));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 300 && hg.wavelengths[1] == 400);
  CHK(hg.g == 0.5);

  CHK(phase = rnsf_get_phase_fn(rnsf, 3));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_DISCRETE);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_BAD_ARG);
  CHK(rnsf_phase_fn_get_discrete(NULL, &discrete) == RES_BAD_ARG);
  CHK(rnsf_phase_fn_get_discrete(phase, NULL) == RES_BAD_ARG);
  CHK(rnsf_phase_fn_get_discrete(phase, &discrete) == RES_OK);
  CHK(discrete.wavelengths[0] == 850);
  CHK(discrete.wavelengths[1] == 875.123);
  CHK(discrete.nitems == 5);
  CHK(discrete.items[0].theta == 0);
  CHK(eq_eps(discrete.items[1].theta, PI/4.0, 1.e-6));
  CHK(eq_eps(discrete.items[2].theta, PI/2.0, 1.e-6));
  CHK(eq_eps(discrete.items[3].theta, 3*PI/4.0, 1.e-6));
  CHK(eq_eps(discrete.items[4].theta, PI, 1.e-6));
  /* Check normalization */
  CHK(eq_eps(discrete.items[0].value, 1.0/(4*PI), 1.e-6));
  CHK(eq_eps(discrete.items[1].value, 1.0/(4*PI), 1.e-6));
  CHK(eq_eps(discrete.items[2].value, 1.0/(4*PI), 1.e-6));
  CHK(eq_eps(discrete.items[3].value, 1.0/(4*PI), 1.e-6));

  CHK(phase = rnsf_get_phase_fn(rnsf, 4));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 900 && hg.wavelengths[1] == 900);
  CHK(hg.g == -0.1);

  CHK(fclose(fp) == 0);
}

static void
test_fetch(struct rnsf* rnsf)
{
  FILE* fp = NULL;
  size_t iphase_fn = 0;

  CHK(fp = tmpfile());
  fprintf(fp, "bands 0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(rnsf_fetch_phase_fn(NULL, 400, 0.5, &iphase_fn) == RES_BAD_ARG);
  CHK(rnsf_fetch_phase_fn(rnsf, -1, 0.5, &iphase_fn) == RES_BAD_ARG);
  CHK(rnsf_fetch_phase_fn(rnsf, 400, -0.1, &iphase_fn) == RES_BAD_ARG);
  CHK(rnsf_fetch_phase_fn(rnsf, 400, 1, &iphase_fn) == RES_BAD_ARG);
  CHK(rnsf_fetch_phase_fn(rnsf, 400, 0.5, NULL) == RES_BAD_ARG);
  CHK(rnsf_fetch_phase_fn(rnsf, 400, 0.5, &iphase_fn) == RES_OK);
  CHK(iphase_fn >= rnsf_get_phase_fn_count(rnsf));

  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "200 300 HG 0.0\n");
  fprintf(fp, "300 400 HG 0.1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(rnsf_fetch_phase_fn(rnsf, 100, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 450, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 300, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 300.1, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);

  CHK(fp = tmpfile());
  fprintf(fp, "bands 4\n");
  fprintf(fp, "100 150 HG 0\n");
  fprintf(fp, "200 200 HG 1\n");
  fprintf(fp, "300 400 HG -1\n");
  fprintf(fp, "400 401 discrete 4\n");
  fprintf(fp, " 0 1\n");
  fprintf(fp, " 0.5 1\n");
  fprintf(fp, " 1.57 1\n");
  fprintf(fp, " 3.14159 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(rnsf_fetch_phase_fn(rnsf, 160, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 160, 0.5, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 160, 0.8, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 200, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 250, 0.49, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 250, 0.5, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 2);
  CHK(rnsf_fetch_phase_fn(rnsf, 400.1, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 3);
}

static void
test_load_fail(struct rnsf* rnsf)
{
  FILE* fp = NULL;

  /* Invalid keyword */
  CHK(fp = tmpfile());
  fprintf(fp, "bandes 1\n");
  fprintf(fp, "380 780 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* No band count */
  CHK(fp = tmpfile());
  fprintf(fp, "bands\n");
  fprintf(fp, "380 780 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing a band definition */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "380 780 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid wavelengths */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "-380 780 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing a band boundary */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid phase function type */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 Henyey-Greenstein 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid asymmetric param  */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 HG 1.01\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Additional parameters. Print a warning */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1 additional_text\n");
  fprintf(fp, "380 780 HG 1.0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  /* Unsorted phase functions */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "380 400 HG 1.0\n");
  fprintf(fp, "280 300 HG 0.0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Phase functions overlap */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "280 400 HG 1.0\n");
  fprintf(fp, "300 480 HG 0.0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Phase functions overlap */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 2\n");
  fprintf(fp, "280 280 HG 1.0\n");
  fprintf(fp, "280 280 HG 0.0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid #angles */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 discrete 1\n");
  fprintf(fp, "  0 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid last angle */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 discrete 2\n");
  fprintf(fp, "  0 1\n");
  fprintf(fp, "  3.14 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing an angle */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 discrete 2\n");
  fprintf(fp, "  0 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid angle */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 discrete 3\n");
  fprintf(fp, "  0 1\n");
  fprintf(fp, "  3.14159 1\n");
  fprintf(fp, "  4 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Unsorted angles */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 discrete 5\n");
  fprintf(fp, "  0 1\n");
  fprintf(fp, "  0.1 1\n");
  fprintf(fp, "  0.2 1\n");
  fprintf(fp, "  0.15 1\n");
  fprintf(fp, "  3.14159 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Additional text. Print a warning */
  CHK(fp = tmpfile());
  fprintf(fp, "bands 1\n");
  fprintf(fp, "380 780 discrete 4\n");
  fprintf(fp, "  0 1\n");
  fprintf(fp, "  0.1 1\n");
  fprintf(fp, "  0.15 1 additional_text\n");
  fprintf(fp, "  3.1416 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);
}

int
main(int argc, char** argv)
{
  struct rnsf_create_args args = RNSF_CREATE_ARGS_DEFAULT;
  struct rnsf* rnsf = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(rnsf_create(&args, &rnsf) == RES_OK);
  CHK(rnsf_get_phase_fn_count(rnsf) == 0);

  test_load1(rnsf);
  test_load2(rnsf);
  test_fetch(rnsf);
  test_load_fail(rnsf);

  CHK(rnsf_ref_put(rnsf) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
