/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnsf.h"
#include "rnsf_c.h"
#include "rnsf_log.h"
#include "rnsf_phase_fn.h"

#include <rsys/algorithm.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE int
cmp_phase(const void* key, const void* item)
{
  const double wavelength = (*(const double*)key);
  const struct rnsf_phase_fn* phase = item;
  if(wavelength < phase->wlen[0]) {
    return -1;
  } else if(wavelength > phase->wlen[1]) {
    return 1;
  } else {
    return 0;
  }
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
rnsf_fetch_phase_fn
  (const struct rnsf* rnsf,
   const double wavelength,
   const double sample, /* In [0, 1[ */
   size_t* out_iphase_fn)
{
  const struct rnsf_phase_fn* phases = NULL;
  const struct rnsf_phase_fn* phase = NULL;
  size_t nphases = 0;
  size_t iphase_fn = 0;
  res_T res = RES_OK;

  if(!rnsf
  || wavelength < 0
  || sample < 0
  || sample >= 1
  || !out_iphase_fn) {
    res = RES_BAD_ARG;
    goto error;
  }

  phases = darray_phase_fn_cdata_get(&rnsf->phases);
  nphases = darray_phase_fn_size_get(&rnsf->phases);

  if(!nphases) {
    log_warn(rnsf, "%s: no phase function to fetch.\n", FUNC_NAME);
    *out_iphase_fn = 0;
    goto exit;
  }

  phase = search_lower_bound
    (&wavelength, phases, nphases, sizeof(*phases), cmp_phase);

  /* All phase functions are set for a wavelength greater or equal to the
   * submitted wavelength */
  if(phase == phases) {
    ASSERT(phases[0].wlen[0] > wavelength 
       || (phases[0].wlen[0] <= wavelength && wavelength <= phases[0].wlen[1]));
    iphase_fn = 0;
  }

  /* All phase functions are set for a wavelength less than the submitted
   * wavelength */
  else if(!phase) {
    ASSERT(phases[nphases-1].wlen[1] < wavelength);
    iphase_fn = nphases - 1;
  }

  /* The wavelength range of the found phase function overlaps the submitted
   * wavelength */
  else if(wavelength >= phase->wlen[0] && wavelength <= phase->wlen[1]) {
    iphase_fn = (size_t)(phase - phases);
  }

  /* The found phase function is defined for a wavelength greater or equal to
   * the submitted wavelength */
  else {
    /* Compute the linear interpolation parameter that defines the submitted
     * wavelength wrt to the wavelenths boundaries of the phase functions
     * surrounding it */
    const struct rnsf_phase_fn* phase0 = phase-1;
    const struct rnsf_phase_fn* phase1 = phase;
    const double u =
      (phase1->wlen[0] - wavelength)
    / (phase1->wlen[0] - phase0->wlen[1]);

    /* Consider the linear interplation parameter previously defined as a
     * probability and use the submitted sample to select one of the phase
     * function surrounding the submitted wavelength. */
    if(sample < u) {
      iphase_fn = (size_t)(phase - phases - 1);
    } else {
      iphase_fn = (size_t)(phase - phases);
    }
  }

  ASSERT(iphase_fn < nphases);
  *out_iphase_fn = iphase_fn;

exit:
  return res;
error:
  goto exit;
}
