/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnsf.h"

#include <rsys/math.h>
#include <rsys/mem_allocator.h>

static void
test_load(struct rnsf* rnsf)
{
  struct rnsf_phase_fn_discrete discrete = RNSF_PHASE_FN_DISCRETE_NULL;
  struct rnsf_phase_fn_hg hg = RNSF_PHASE_FN_HG_NULL;
  const struct rnsf_phase_fn* phase = NULL;
  FILE* fp = NULL;

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(rnsf_get_phase_fn_count(rnsf) == 0);
  CHK(fclose(fp) == 0);

  CHK(fp = tmpfile());
  fprintf(fp, "# Comment\n");
  fprintf(fp, "wavelengths 1\n");
  fprintf(fp, "\n");
  fprintf(fp, "200.1 HG 0\n");
  rewind(fp);

  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);

  CHK(rnsf_get_phase_fn_count(rnsf) == 1);
  CHK(phase = rnsf_get_phase_fn(rnsf, 0));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);

  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 200.1);
  CHK(hg.wavelengths[1] == 200.1);
  CHK(hg.g == 0);
  CHK(fclose(fp) == 0);

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 10\n");
  fprintf(fp, "\n");
  fprintf(fp, "# Short waves\n");
  fprintf(fp, "430 discrete 8\n");
  fprintf(fp, "  0       0.02\n");
  fprintf(fp, "  0.23    0.04\n");
  fprintf(fp, "  0.5     0.07\n");
  fprintf(fp, "  0.7     0.15\n");
  fprintf(fp, "  1.54    1.23\n");
  fprintf(fp, "  1.8     0.02\n");
  fprintf(fp, "  2       1.23\n");
  fprintf(fp, "  3.14159 0.79\n");
  fprintf(fp, "450 discrete 2\n");
  fprintf(fp, "  0        0.5\n");
  fprintf(fp, "  3.14159 0.796\n");
  fprintf(fp, "750 discrete 5\n");
  fprintf(fp, "  0  %.9g\n", 1.0/(4.0*PI));
  fprintf(fp, "  %.9g %.9g\n", PI/4.0, 1.0/(4.0*PI));
  fprintf(fp, "  %.9g %.9g\n", PI/2.0, 1.0/(4.0*PI));
  fprintf(fp, "  %.9g %.9g\n", 3*PI/4.0, 1.0/(4.0*PI));
  fprintf(fp, "  %g %.9g\n", PI, 1.0/(4.0*PI));
  fprintf(fp, "\n");
  fprintf(fp, "# Long waves\n");
  fprintf(fp, "1100    HG   -0.1\n");
  fprintf(fp, "1300    HG    0.57\n");
  fprintf(fp, "1400    HG    0.4\n");
  fprintf(fp, "2100    HG    0.3\n");
  fprintf(fp, "2500    HG   -0.9\n");
  fprintf(fp, "2900    HG   -0.4\n");
  fprintf(fp, "100000  HG    0.0\n");
  rewind(fp);

  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(rnsf_get_phase_fn_count(rnsf) == 10);

  CHK(phase = rnsf_get_phase_fn(rnsf, 0));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_DISCRETE);
  CHK(rnsf_phase_fn_get_discrete(phase, &discrete) == RES_OK);
  CHK(discrete.wavelengths[0] == 430);
  CHK(discrete.wavelengths[1] == 430);
  CHK(discrete.nitems == 8);
  CHK(discrete.items[0].theta == 0);
  CHK(discrete.items[1].theta == 0.23);
  CHK(discrete.items[2].theta == 0.5);
  CHK(discrete.items[3].theta == 0.7);
  CHK(discrete.items[4].theta == 1.54);
  CHK(discrete.items[5].theta == 1.8);
  CHK(discrete.items[6].theta == 2);
  CHK(eq_eps(discrete.items[7].theta, PI, 1.e-6));

  CHK(phase = rnsf_get_phase_fn(rnsf, 1));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_DISCRETE);
  CHK(rnsf_phase_fn_get_discrete(phase, &discrete) == RES_OK);
  CHK(discrete.wavelengths[0] == 450);
  CHK(discrete.wavelengths[1] == 450);
  CHK(discrete.nitems == 2);
  CHK(discrete.items[0].theta == 0);
  CHK(eq_eps(discrete.items[1].theta, PI, 1.e-6));

  CHK(phase = rnsf_get_phase_fn(rnsf, 2));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_DISCRETE);
  CHK(rnsf_phase_fn_get_discrete(phase, &discrete) == RES_OK);
  CHK(discrete.wavelengths[0] == 750);
  CHK(discrete.wavelengths[1] == 750);
  CHK(discrete.nitems == 5);
  CHK(discrete.items[0].theta == 0);
  CHK(eq_eps(discrete.items[1].theta, PI/4.0, 1.e-6));
  CHK(eq_eps(discrete.items[2].theta, PI/2.0, 1.e-6));
  CHK(eq_eps(discrete.items[3].theta, 3*PI/4.0, 1.e-6));
  CHK(eq_eps(discrete.items[4].theta, PI, 1.e-6));
  CHK(eq_eps(discrete.items[0].value, 1/(4.0*PI), 1.e-6));
  CHK(eq_eps(discrete.items[1].value, 1/(4.0*PI), 1.e-6));
  CHK(eq_eps(discrete.items[2].value, 1/(4.0*PI), 1.e-6));
  CHK(eq_eps(discrete.items[3].value, 1/(4.0*PI), 1.e-6));
  CHK(eq_eps(discrete.items[4].value, 1/(4.0*PI), 1.e-6));

  CHK(phase = rnsf_get_phase_fn(rnsf, 3));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 1100);
  CHK(hg.wavelengths[1] == 1100);
  CHK(hg.g == -0.1);

  CHK(phase = rnsf_get_phase_fn(rnsf, 4));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 1300);
  CHK(hg.wavelengths[1] == 1300);
  CHK(hg.g == 0.57);

  CHK(phase = rnsf_get_phase_fn(rnsf, 5));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 1400);
  CHK(hg.wavelengths[1] == 1400);
  CHK(hg.g == 0.4);

  CHK(phase = rnsf_get_phase_fn(rnsf, 6));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 2100);
  CHK(hg.wavelengths[1] == 2100);
  CHK(hg.g == 0.3);

  CHK(phase = rnsf_get_phase_fn(rnsf, 7));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 2500);
  CHK(hg.wavelengths[1] == 2500);
  CHK(hg.g == -0.9);

  CHK(phase = rnsf_get_phase_fn(rnsf, 8));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 2900);
  CHK(hg.wavelengths[1] == 2900);
  CHK(hg.g == -0.4);

  CHK(phase = rnsf_get_phase_fn(rnsf, 9));
  CHK(rnsf_phase_fn_get_type(phase) == RNSF_PHASE_FN_HG);
  CHK(rnsf_phase_fn_get_hg(phase, &hg) == RES_OK);
  CHK(hg.wavelengths[0] == 100000);
  CHK(hg.wavelengths[1] == 100000);
  CHK(hg.g == 0);

  CHK(fclose(fp) == 0);
}

static void
test_fetch(struct rnsf* rnsf)
{
  FILE* fp = NULL;
  size_t iphase_fn = 0;

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 400, 0.5, &iphase_fn) == RES_OK);
  CHK(iphase_fn >= rnsf_get_phase_fn_count(rnsf));

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "200 HG 0\n");
  fprintf(fp, "300 HG 0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(rnsf_fetch_phase_fn(rnsf, 299, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 301, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 200, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 300, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 280, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 280, 0.19, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 280, 0.20, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);

  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 4\n");
  fprintf(fp, "100 HG 0\n");
  fprintf(fp, "200 HG 1\n");
  fprintf(fp, "300 HG -1\n");
  fprintf(fp, "400 discrete 4\n");
  fprintf(fp, " 0 1\n");
  fprintf(fp, " 0.5 1\n");
  fprintf(fp, " 1.57 1\n");
  fprintf(fp, " 3.1416 1\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);

  CHK(rnsf_fetch_phase_fn(rnsf, 50, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 0);
  CHK(rnsf_fetch_phase_fn(rnsf, 120, 0.8, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 270, 0.29, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 1);
  CHK(rnsf_fetch_phase_fn(rnsf, 270, 0.3, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 2);
  CHK(rnsf_fetch_phase_fn(rnsf, 350, 0.5, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 3);
  CHK(rnsf_fetch_phase_fn(rnsf, 450, 0, &iphase_fn) == RES_OK);
  CHK(iphase_fn == 3);
}

static void
test_load_fail(struct rnsf* rnsf)
{
  FILE* fp = NULL;

  /* No wavelength count */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths\n");
  fprintf(fp, "380 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Missing a phase function */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "380 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid wavelength */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 1\n");
  fprintf(fp, "-380 HG 0.5\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Unsorted phase functions */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "380 HG 0.5\n");
  fprintf(fp, "280 HG 0.0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Phase functions overlap */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2\n");
  fprintf(fp, "280 HG 0.5\n");
  fprintf(fp, "280 HG 0.0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Additional text. Print a warning */
  CHK(fp = tmpfile());
  fprintf(fp, "wavelengths 2 additional_text\n");
  fprintf(fp, "280 HG 0.5\n");
  fprintf(fp, "380 HG 0.0\n");
  rewind(fp);
  CHK(rnsf_load_stream(rnsf, fp, NULL) == RES_OK);
  CHK(fclose(fp) == 0);
}

int
main(int argc, char** argv)
{
  struct rnsf_create_args args = RNSF_CREATE_ARGS_DEFAULT;
  struct rnsf* rnsf = NULL;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(rnsf_create(&args, &rnsf) == RES_OK);
  CHK(rnsf_get_phase_fn_count(rnsf) == 0);

  test_load(rnsf);
  test_fetch(rnsf);
  test_load_fail(rnsf);

  CHK(rnsf_ref_put(rnsf) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
