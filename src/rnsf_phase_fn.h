/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNSF_PHASE_FN_H
#define RNSF_PHASE_FN_H

#include "rnsf.h"
#include <rsys/dynamic_array.h>

struct phase_fn_hg {
  double g; /* Asymmetric parameter in [-1, 1] */
};

/* Generate the dynamic array of discrete items */
#define DARRAY_NAME discrete_item
#define DARRAY_DATA struct rnsf_discrete_item
#include <rsys/dynamic_array.h>

struct phase_fn_discrete {
  struct darray_discrete_item items;
};

static INLINE res_T
phase_fn_discrete_init
  (struct mem_allocator* allocator,
   struct phase_fn_discrete* phase)
{
  ASSERT(phase);
  darray_discrete_item_init(allocator, &phase->items);
  return RES_OK;
}

static INLINE void
phase_fn_discrete_release(struct phase_fn_discrete* phase)
{
  ASSERT(phase);
  darray_discrete_item_release(&phase->items);
}

static INLINE res_T
phase_fn_discrete_copy
  (struct phase_fn_discrete* dst,
   const struct phase_fn_discrete* src)
{
  ASSERT(src && dst);
  return darray_discrete_item_copy(&dst->items, &src->items);
}

static INLINE res_T
phase_fn_discrete_copy_and_release
  (struct phase_fn_discrete* dst,
   struct phase_fn_discrete* src)
{
  ASSERT(src && dst);
  return darray_discrete_item_copy_and_release(&dst->items, &src->items);
}

struct rnsf_phase_fn {
  double wlen[2]; /* In nanometers */
  enum rnsf_phase_fn_type type;
  union {
    struct phase_fn_hg hg;
    struct phase_fn_discrete discrete;
  } param;
};

static INLINE res_T
phase_fn_init
  (struct mem_allocator* allocator,
   struct rnsf_phase_fn* phase)
{
  ASSERT(phase);
  (void)allocator;
  phase->wlen[0] = 0;
  phase->wlen[1] = 0;
  phase->type = RNSF_PHASE_FN_NONE__;
  return RES_OK;
}

static INLINE void
phase_fn_release(struct rnsf_phase_fn* phase)
{
  ASSERT(phase);
  switch(phase->type) {
    case RNSF_PHASE_FN_HG:
    case RNSF_PHASE_FN_NONE__:
      /* Do nothing */
      break;
    case RNSF_PHASE_FN_DISCRETE:
      phase_fn_discrete_release(&phase->param.discrete);
      break;
    default: FATAL("Unreachable code.\n"); break;
  }
}

static INLINE res_T
phase_fn_copy
  (struct rnsf_phase_fn* dst,
   const struct rnsf_phase_fn* src)
{
  res_T res = RES_OK;
  dst->wlen[0] = src->wlen[0];
  dst->wlen[1] = src->wlen[1];
  dst->type = src->type;
  switch(src->type) {
    case RNSF_PHASE_FN_HG:
      dst->param.hg.g = src->param.hg.g;
      break;
    case RNSF_PHASE_FN_DISCRETE:
      res = phase_fn_discrete_copy
        (&dst->param.discrete,
         &src->param.discrete);
      break;
    case RNSF_PHASE_FN_NONE__: /* Do nothing */ break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return res;
}

static INLINE res_T
phase_fn_copy_and_release
  (struct rnsf_phase_fn* dst,
   struct rnsf_phase_fn* src)
{
  res_T res = RES_OK;
  dst->wlen[0] = src->wlen[0];
  dst->wlen[1] = src->wlen[1];
  dst->type = src->type;
  switch(src->type) {
    case RNSF_PHASE_FN_HG:
      dst->param.hg.g = src->param.hg.g;
      break;
    case RNSF_PHASE_FN_DISCRETE:
      res = phase_fn_discrete_copy_and_release
        (&dst->param.discrete,
         &src->param.discrete);
      break;
    case RNSF_PHASE_FN_NONE__: /* Do nothing */ break;
    default: FATAL("Unreachable code.\n"); break;
  }
  return res;
}

/* Generate the dynamic array of phase functions */
#define DARRAY_NAME phase_fn
#define DARRAY_DATA struct rnsf_phase_fn
#define DARRAY_FUNCTOR_INIT phase_fn_init
#define DARRAY_FUNCTOR_RELEASE phase_fn_release
#define DARRAY_FUNCTOR_COPY phase_fn_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE phase_fn_copy_and_release
#include <rsys/dynamic_array.h>

#endif /* RNSF_PHASE_FN_H */
