/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef RNSF_H
#define RNSF_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(RNSF_SHARED_BUILD) /* Build shared library */
  #define RNSF_API extern EXPORT_SYM
#elif defined(RNSF_STATIC) /* Use/build static library */
  #define RNSF_API extern LOCAL_SYM
#else /* Use shared library */
  #define RNSF_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the htcp function `Func'
 * returns an error. One should use this macro on htcp function calls for
 * which no explicit error checking is performed */
#ifndef NDEBUG
  #define RNSF(Func) ASSERT(rnsf_ ## Func == RES_OK)
#else
  #define RNSF(Func) rnsf_ ## Func
#endif

/* Forward declaration of external data types */
struct logger;
struct mem_allocator;

enum rnsf_phase_fn_type {
  RNSF_PHASE_FN_HG,
  RNSF_PHASE_FN_DISCRETE,
  RNSF_PHASE_FN_NONE__
};

struct rnsf_create_args {
  struct logger* logger; /* NULL <=> use default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define RNSF_CREATE_ARGS_DEFAULT__ {NULL, NULL, 0}
static const struct rnsf_create_args RNSF_CREATE_ARGS_DEFAULT =
  RNSF_CREATE_ARGS_DEFAULT__;

/* Henyey & Greestein phase function */
struct rnsf_phase_fn_hg {
  double wavelengths[2]; /* Inclusive wavelength range in nanometers */
  double g; /* Asymmetric parameter in [-1, 1] */
};
#define RNSF_PHASE_FN_HG_NULL__ {{0,0}, 0}
static const struct rnsf_phase_fn_hg RNSF_PHASE_FN_HG_NULL =
  RNSF_PHASE_FN_HG_NULL__;

struct rnsf_discrete_item {
  double theta; /* In radian */
  double value;
};
#define RNSF_DISCRETE_ITEM_NULL__ {0,0}
static const struct rnsf_discrete_item RNSF_DISCRETE_ITEM_NULL =
  RNSF_DISCRETE_ITEM_NULL__;

/* Discrete phase function */
struct rnsf_phase_fn_discrete {
  double wavelengths[2]; /* Inclusive wavelength range in nanometers */
  size_t nitems;
  const struct rnsf_discrete_item* items;
};
#define RNSF_PHASE_FN_DISCRETE_NULL__ {{0,0}, 0, NULL}
static const struct rnsf_phase_fn_discrete RNSF_PHASE_FN_DISCRETE_NULL =
  RNSF_PHASE_FN_DISCRETE_NULL__;

/* Opaque data types */
struct rnsf;
struct rnsf_phase_fn;

BEGIN_DECLS

/*******************************************************************************
 * Rad-Net Scattering Functions API
 ******************************************************************************/
RNSF_API res_T
rnsf_create
  (const struct rnsf_create_args* args,
   struct rnsf** rnsf);

RNSF_API res_T
rnsf_ref_get
  (struct rnsf* rnsf);

RNSF_API res_T
rnsf_ref_put
  (struct rnsf* rnsf);

RNSF_API res_T
rnsf_load
  (struct rnsf* rnsf,
   const char* path);

RNSF_API res_T
rnsf_load_stream
  (struct rnsf* rnsf,
   FILE* stream,
   const char* stream_name); /* May be NULL */

RNSF_API size_t
rnsf_get_phase_fn_count
  (const struct rnsf* rnsf);

RNSF_API const struct rnsf_phase_fn*
rnsf_get_phase_fn
  (const struct rnsf* rnsf,
   const size_t iphase_fn);

RNSF_API enum rnsf_phase_fn_type
rnsf_phase_fn_get_type
  (const struct rnsf_phase_fn* phase_fn);

RNSF_API res_T
rnsf_phase_fn_get_hg
  (const struct rnsf_phase_fn* phase_fn,
   struct rnsf_phase_fn_hg* hg);

RNSF_API res_T
rnsf_phase_fn_get_discrete
  (const struct rnsf_phase_fn* phase_fn,
   struct rnsf_phase_fn_discrete* discrete);

/* Fetch a phase function with respect to the submitted wavelength and the
 * given sample. The phase function is chosen according to the alpha
 * coefficient between [0, 1] defined as below:
 *
 *    alpha = (lambda1 - wavelength) / (lambda1 - lambda0)
 *
 * with lambda0 < lambda1, the phase function wavelengths surrounding the
 * submitted wavelength. The index returned refers to the phase function
 * corresponding to lambda0 or lambda1 depending on whether the given sample is
 * less than or greater than alpha, respectively.
 *
 * If the phase functions are stored by spectral band, lambda0 and lambda1 are
 * the upper boundary of the lower band and the lower boundary of the upper
 * band, respectively, which surround the wavelength. If the wavelength is in a
 * specific band, the index for that band is simply returned.
 *
 * If the wavelength is outside the domain, the index returned refers to either
 * the first or the last phase function depending on whether the wavelength is
 * below or above the domain, respectively. */
RNSF_API res_T
rnsf_fetch_phase_fn
  (const struct rnsf* rnsf,
   const double wavelength,
   const double sample, /* In [0, 1[ */
   size_t* iphase_fn);

END_DECLS

#endif /* RNSF_H */

