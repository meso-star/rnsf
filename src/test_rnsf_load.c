/* Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
 * Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
 * Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
 * Copyright (C) 2022, 2023 |Méso|Star> (contact@meso-star.com)
 * Copyright (C) 2022, 2023 Observatoire de Paris
 * Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
 * Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
 * Copyright (C) 2022, 2023 Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "rnsf.h"
#include <rsys/math.h>
#include <rsys/mem_allocator.h>

static void
check_phase_fn_discrete(const struct rnsf_phase_fn* phase_fn)
{
  struct rnsf_phase_fn_discrete discrete = RNSF_PHASE_FN_DISCRETE_NULL;
  size_t i;

  CHK(rnsf_phase_fn_get_discrete(phase_fn, &discrete) == RES_OK);

  /* !NaN */
  CHK(discrete.wavelengths[0] == discrete.wavelengths[0]);
  CHK(discrete.wavelengths[1] == discrete.wavelengths[1]);
  CHK(discrete.nitems >= 2);

  FOR_EACH(i, 0, discrete.nitems) {
    const struct rnsf_discrete_item* item = discrete.items + i;

    /* !NaN */
    CHK(item->theta == item->theta);
    CHK(item->value == item->value);

    CHK(item->theta >= 0);
    CHK(item->theta < PI || eq_eps(item->theta, PI, 1.e-6));

    if(i > 0) {
      CHK(item[0].theta > item[-1].theta);
    }
  }
}

static void
check_phase_fn_hg(const struct rnsf_phase_fn* phase_fn)
{
  struct rnsf_phase_fn_hg hg = RNSF_PHASE_FN_HG_NULL;

  CHK(rnsf_phase_fn_get_hg(phase_fn, &hg) == RES_OK);

  /* !NaN */
  CHK(hg.wavelengths[0] == hg.wavelengths[0]);
  CHK(hg.wavelengths[1] == hg.wavelengths[1]);
  CHK(hg.g = hg.g);

  CHK(hg.wavelengths[0] <= hg.wavelengths[1]);
  CHK(-1 <= hg.g && hg.g <= 1);
}

int
main(int argc, char** argv)
{
  struct rnsf_create_args args = RNSF_CREATE_ARGS_DEFAULT;
  struct rnsf* rnsf = NULL;
  int i;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(rnsf_create(&args, &rnsf) == RES_OK);

  FOR_EACH(i, 1, argc) {
    size_t iphase_fn;
    size_t nphase_fn;

    printf("Load %s\n", argv[i]);
    CHK(rnsf_load(rnsf, argv[i]) == RES_OK);

    nphase_fn = rnsf_get_phase_fn_count(rnsf);
    FOR_EACH(iphase_fn, 0, nphase_fn) {
      const struct rnsf_phase_fn* phase_fn = rnsf_get_phase_fn(rnsf, iphase_fn);

      switch(rnsf_phase_fn_get_type(phase_fn)) {
        case RNSF_PHASE_FN_DISCRETE:
          check_phase_fn_discrete(phase_fn);
          break;
        case RNSF_PHASE_FN_HG:
          check_phase_fn_hg(phase_fn);
          break;
        default: FATAL("Unreachable code\n"); break;
      }
    }
  }
  CHK(rnsf_ref_put(rnsf) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
