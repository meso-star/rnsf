.\" Copyright (C) 2022, 2023 Centre National de la Recherche Scientifique
.\" Copyright (C) 2022, 2023 Institut Pierre-Simon Laplace
.\" Copyright (C) 2022, 2023 Institut de Physique du Globe de Paris
.\" Copyright (C) 2022, 2023 |Méso|Star>(contact@meso-star.com)
.\" Copyright (C) 2022, 2023 Observatoire de Paris
.\" Copyright (C) 2022, 2023 Université de Reims Champagne-Ardenne
.\" Copyright (C) 2022, 2023 Université de Versaille Saint-Quentin
.\" Copyright (C) 2022, 2023 Université Paul Sabatier
.\"
.\" This program is free software: you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation, either version 3 of the License, or
.\" (at your option) any later version.
.\"
.\" This program is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with this program. If not, see <http://www.gnu.org/licenses/>.
.Dd September 15, 2023
.Dt RNSF 5
.Os
.Sh NAME
.Nm rnsf
.Nd Rad-Net Scattering Functions file format
.Sh DESCRIPTION
.Nm
is a text file format that describes a phase function whose type and parameters
can vary spectrally.
Its data are described for a set of wavelengths or spectral bands that must be
listed in ascending order.
.Pp
Characters behind the hash mark
.Pq Li #
are considered comments and are therefore ignored, as well as empty lines,
i.e. lines without any characters or composed only of spaces and tabs.
.Pp
The file format is as follows:
.Bl -column (per-band-phase-func) (::=) ()
.It Ao Va rnsf Ac Ta ::= Ta Ao Va per-wlen-phase-func Ac |
.Ao Va per-band-phase-func Ac
.It \  Ta Ta
.It Ao  Va per-wlen-phase-func Ac Ta ::= Ta
.Li wavelengths
.Aq Va wavelengths-count
.It Ta Ta Aq Va wlen-phase-func
.It Ta Ta Va ...
.It Ao Va wlen-phase-func Ac Ta ::= Ta Ao Va wavelength Ac Ao Va phase-func Ac
.It Ao Va wavelengths-count Ac Ta ::= Ta Va integer
.It Ao Va wavelength Ac Ta ::= Ta Va real
# In nanometers
.It \  Ta Ta
.It Ao Va per-band-phase-func Ac Ta ::= Ta
.Li bands
.Aq Va bands-count
.It Ta Ta Aq Va band-phase-func
.It Ta Ta Va ...
.It Ao Va band-phase-func Ac Ta ::= Ta
.Aq Va length-min
.Aq Va length-max
.Aq Va phase-func
.It Ao Va bands-count Ac Ta ::= Ta Va integer
.It Ao Va length-min Ac Ta ::= Ta Va real
# Inclusive bound in nanometers
.It Ao Va length-max Ac Ta ::= Ta Va real
# Inclusive bound in nanometers
.It \  Ta Ta
.It Ao Va phase-func Ac Ta ::= Ta Ao Va phase-func-HG Ac |
.Ao Va phase-func-discrete Ac
.It \  Ta Ta
.It Ao Va phase-func-HG Ac Ta ::= Ta Li HG Aq Va asymmetric-param
.It Ao Va asymmetric-param Ac Ta ::= Ta Va real
# In
.Bq -1, 1
.It \  Ta Ta
.It Ao Va phase-func-discrete Ac Ta ::= Ta Li discrete Aq Va angles-count
.It Ta Ta Li 0 Aq Va value
.It Ta Ta Op Ao Va pair Ac Va ...
# Ascending angles
.It Ta Ta Li 3.14159 Aq Va value
.It Ao Va angles-count Ac Ta ::= Ta Va integer
# Must be >= 2
.It Ao Va pair Ac Ta ::= Ta Ao Va theta Ac Ao Va value Ac
.It Ao Va theta Ac Ta ::= Ta Va real
# In radians
.It Ao Va value Ac Ta ::= Ta Va real
# Not necessarily normalized
.El
.Sh EXAMPLES
Spectrally varying phase function on two spectral bands: a band for the visible
part of the spectrum for which a Henyey & Greenstein phase function is used,
and a band for long waves with a discretized phase function on 4 angles:
.Bd -literal -offset Ds
bands 2

# Visible part
380 780 HG 0

# Inrared
1000 100000 discrete 4
  0       0.079577
  0.78    0.079577
  2.35    0.079577
  3.14159 0.079577
.Ed
.Pp
Phase function for a set of 10 wavelengths.
Use a discrete phase function for short waves and Henyey & Greenstein for long
waves:
.Bd -literal -offset Ds
wavelengths 10

# Short waves
430 discrete 8
  0           0.02
  0.23        0.04
  0.5         0.07
  0.7         0.15
  1.54        1.23
  1.8         0.02
  2           1.23
  3.14159     0.79
450 discrete 2
  0           0.5
  3.14159     0.796
750 discrete 4
  0           0.079577
  0.78        0.079577
  2.35        0.079577
  3.14159     0.079577

# Long waves
1100    HG   -0.1
1300    HG    0.57
1400    HG    0.4
2100    HG    0.3
2500    HG   -0.9
2900    HG   -0.4
100000  HG    0.0
.Ed
.Sh HISTORY
The
.Nm
format was first developed for the
.Xr htrdr-planeto 1
program.
